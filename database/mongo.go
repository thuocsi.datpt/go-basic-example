package database

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DB database object
var DB *mongo.Database

// ConnectDB returns mongodb connection
func ConnectDB() {
	// Set client options
	mongoURI := "mongodb+srv://DatPham:datpham2001@cluster0.cs9dq.mongodb.net/golangDB?retryWrites=true&w=majority"
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		panic(err)
	}

	// Set timeout 5 second wanted to use when trying to connect
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		panic(err)
	}

	// Check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		panic(err)
	}

	DB = client.Database("golangDB")
	fmt.Println("Connect to MongoDB successfully")
}
