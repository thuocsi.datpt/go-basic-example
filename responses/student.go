package responses

import "github.com/labstack/echo/v4"

// StudentResponse create student's response structure
type StudentResponse struct {
	Status  int       `json:"status"`
	Message string    `json:"message"`
	Data    *echo.Map `json:"data"`
}
