package domain

import (
	"context"
	"net/http"
	"time"

	"simple-login-api/database"
	"simple-login-api/responses"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var validate = validator.New()

// convertToBson ...
func convertToBson(ent interface{}) (bson.M, error) {
	if ent == nil {
		return bson.M{}, nil
	}

	sel, err := bson.Marshal(ent)
	if err != nil {
		return nil, err
	}

	obj := bson.M{}
	bson.Unmarshal(sel, &obj)

	return obj, nil
}

// CreateStudent create student information
func CreateStudent(c echo.Context) error {
	collection := database.DB.Collection("student")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var req CreateStudentRequestDto
	defer cancel()

	// validate the request body
	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, responses.StudentResponse{Status: http.StatusBadRequest, Message: "error when creating student", Data: &echo.Map{"error": err.Error()}})
	}

	// validate the required fields using validator library
	if errValidator := validate.Struct(&req); errValidator != nil {
		return c.JSON(http.StatusBadRequest, responses.StudentResponse{Status: http.StatusBadRequest, Message: "error when validate student", Data: &echo.Map{"error": errValidator.Error()}})
	}

	student := Student{
		ID:          primitive.NewObjectID(),
		Name:        req.Name,
		DateOfBirth: req.DateOfBirth,
		Gender:      req.Gender,
		Location:    req.Location,
	}

	res, err := collection.InsertOne(ctx, student)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, responses.StudentResponse{Status: http.StatusInternalServerError, Message: "error", Data: &echo.Map{"error": err.Error()}})
	}

	return c.JSON(http.StatusCreated, responses.StudentResponse{Status: http.StatusCreated, Message: "successfully created", Data: &echo.Map{"result": res}})
}

// GetStudent get information of a student
func GetStudent(c echo.Context) error {
	collection := database.DB.Collection("student")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var student Student
	defer cancel()

	// get the student's id from URL parameter
	studentID, _ := primitive.ObjectIDFromHex(c.Param("studentId"))
	filter := &Student{
		ID: studentID,
	}

	bsonObject, err := convertToBson(filter)
	err = collection.FindOne(ctx, bsonObject).Decode(&student)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, responses.StudentResponse{Status: http.StatusInternalServerError, Message: "error when get a student by id", Data: &echo.Map{"data": err.Error()}})
	}

	return c.JSON(http.StatusOK, responses.StudentResponse{Status: http.StatusOK, Message: "success", Data: &echo.Map{"result": student}})
}

// UpdateStudent update info of specific student
func UpdateStudent(c echo.Context) error {
	collection := database.DB.Collection("student")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var req UpdateStudentRequestDto
	defer cancel()

	studentID, _ := primitive.ObjectIDFromHex(c.Param("studentId"))

	// validate the request body
	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, responses.StudentResponse{Status: http.StatusBadRequest, Message: "error", Data: &echo.Map{"error": err.Error()}})
	}

	// use the validator library to validate required fields
	if validationErr := validate.Struct(&req); validationErr != nil {
		return c.JSON(http.StatusBadRequest, responses.StudentResponse{Status: http.StatusBadRequest, Message: "error", Data: &echo.Map{"error": validationErr.Error()}})
	}

	filter := &Student{
		ID: studentID,
	}

	updater := &Student{
		ID:          studentID,
		Name:        req.Name,
		DateOfBirth: req.DateOfBirth,
		Gender:      req.Gender,
		Location:    req.Location,
	}

	bsonObject, err := convertToBson(filter)
	updated, err := collection.UpdateOne(ctx, bsonObject, bson.M{"$set": updater})
	if err != nil {
		return c.JSON(http.StatusBadRequest, responses.StudentResponse{Status: http.StatusBadRequest, Message: "error", Data: &echo.Map{"error": err.Error()}})
	}

	var res Student
	if updated.MatchedCount == 1 {
		err := collection.FindOne(ctx, bson.M{"id": studentID}).Decode(&res)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, responses.StudentResponse{Status: http.StatusInternalServerError, Message: "error", Data: &echo.Map{"error": err.Error()}})
		}

		return c.JSON(http.StatusOK, responses.StudentResponse{Status: http.StatusOK, Message: "success", Data: &echo.Map{"result": res}})
	}

	return c.JSON(http.StatusBadRequest, responses.StudentResponse{Status: http.StatusBadRequest, Message: "error", Data: &echo.Map{"error": "bad"}})
}

// DeleteAStudent delete a student from database
func DeleteAStudent(c echo.Context) error {
	collection := database.DB.Collection("student")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	studentID, _ := primitive.ObjectIDFromHex(c.Param("studentId"))
	filter := &Student{
		ID: studentID,
	}

	bsonObject, err := convertToBson(filter)
	res, err := collection.DeleteOne(ctx, bsonObject)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, responses.StudentResponse{Status: http.StatusInternalServerError, Message: "error", Data: &echo.Map{"error": err.Error()}})
	}

	if res.DeletedCount < 1 {
		return c.JSON(http.StatusNotFound, responses.StudentResponse{Status: http.StatusNotFound, Message: "error", Data: &echo.Map{"error": "Student with specified ID not found!"}})
	}

	return c.JSON(http.StatusOK, responses.StudentResponse{Status: http.StatusOK, Message: "success", Data: &echo.Map{"result": "Student successfully deleted"}})
}

// GetAllStudents get all student from database
func GetAllStudents(c echo.Context) error {
	collection := database.DB.Collection("student")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var students []Student
	defer cancel()

	res, err := collection.Find(ctx, bson.M{})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, responses.StudentResponse{Status: http.StatusInternalServerError, Message: "error", Data: &echo.Map{"error": err.Error()}})
	}

	// reading student from database
	defer res.Close(ctx)
	for res.Next(ctx) {
		var student Student
		if err := res.Decode(&student); err != nil {
			return c.JSON(http.StatusInternalServerError, responses.StudentResponse{Status: http.StatusInternalServerError, Message: "error", Data: &echo.Map{"error": err.Error()}})
		}

		students = append(students, student)
	}

	return c.JSON(http.StatusOK, responses.StudentResponse{Status: http.StatusOK, Message: "success", Data: &echo.Map{"result": students}})
}
