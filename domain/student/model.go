package domain

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// DateTime create date structure
type DateTime struct {
	Day   int64 `bson:"day,omitempty"`
	Month int64 `bson:"month,omitempty"`
	Year  int64 `bson:"year,omitempty"`
}

// CreateStudentRequestDto create student structure
type CreateStudentRequestDto struct {
	DateOfBirth *DateTime `json:"dateOfBirth,omitempty"`
	Name        string    `json:"name,omitempty" validate:"required"`
	Gender      string    `json:"gender,omitempty"`
	Location    string    `json:"location,omitempty"`
}

// CreateStudentResponseDto create student response structure
type CreateStudentResponseDto struct {
	ID          primitive.ObjectID `json:"id"`
	DateOfBirth *DateTime          `json:"dateOfBirth,omitempty"`
	Name        string             `json:"name,omitempty" validate:"required"`
	Gender      string             `json:"gender,omitempty"`
	Location    string             `json:"location,omitempty"`
}

// UpdateStudentRequestDto update student structure
type UpdateStudentRequestDto struct {
	DateOfBirth *DateTime `json:"dateOfBirth,omitempty"`
	Name        string    `json:"name,omitempty" validate:"required"`
	Gender      string    `json:"gender,omitempty"`
	Location    string    `json:"location,omitempty"`
}

// UpdateStudentResponseDto update student structure
type UpdateStudentResponseDto struct {
	DateOfBirth *DateTime `json:"dateOfBirth,omitempty"`
	Name        string    `json:"name,omitempty" validate:"required"`
	Gender      string    `json:"gender,omitempty"`
	Location    string    `json:"location,omitempty"`
}

// Student create student structure
type Student struct {
	ID          primitive.ObjectID `bson:"id,omitempty" json:"id,omitempty"`
	DateOfBirth *DateTime          `bson:"dateOfBirth,omitempty" json:"dateOfBirth,omitempty"`
	Name        string             `bson:"name,omitempty" validate:"required" json:"name,omitempty"`
	Gender      string             `bson:"gender,omitempty" json:"gender,omitempty"`
	Location    string             `bson:"location,omitempty" json:"location,omitempty"`
}
