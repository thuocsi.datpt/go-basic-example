package app

import (
	"simple-login-api/database"

	"github.com/labstack/echo/v4"
)

// StartApp start app with connect database
func StartApp(e *echo.Echo) {
	StudentRoute(e)
	database.ConnectDB()
	e.Logger.Fatal(e.Start(":8080"))
}
