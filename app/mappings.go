package app

import (
	student "simple-login-api/domain/student"

	"github.com/labstack/echo/v4"
)

// StudentRoute create route for student
func StudentRoute(e *echo.Echo) {
	e.POST("/student", student.CreateStudent)
	e.GET("/student/:studentId", student.GetStudent)
	e.PUT("/student/:studentId", student.UpdateStudent)
	e.DELETE("/student/:studentId", student.DeleteAStudent)
	e.GET("/students", student.GetAllStudents)
}
