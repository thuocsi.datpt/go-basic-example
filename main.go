package main

import (
	"simple-login-api/app"

	"github.com/labstack/echo/v4"
)

func main() {
	server := echo.New()

	// run program
	app.StartApp(server)
}
